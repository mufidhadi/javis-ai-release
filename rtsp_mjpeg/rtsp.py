import cv2
import zmq
import threading
from urllib.parse import urlparse, parse_qs

def video_stream(filename):
    cap = cv2.VideoCapture(filename)

    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break

        encoded_frame = cv2.imencode('.jpg', frame)[1].tobytes()
        yield encoded_frame

    cap.release()

def rtsp_server(rtsp_port, video_filename):
    context = zmq.Context()
    socket = context.socket(zmq.PUB)
    socket.bind(f"tcp://*:{rtsp_port}")

    for frame in video_stream(video_filename):
        socket.send(frame)

if __name__ == "__main__":
    rtsp_port = 8554  # Change this to your desired RTSP port

    def handle_rtsp_request(rtsp_url):
        parsed_url = urlparse(rtsp_url)
        query_params = parse_qs(parsed_url.query)
        
        video_filename = query_params.get("filename", [""])[0]
        
        if video_filename:
            server_thread = threading.Thread(target=rtsp_server, args=(rtsp_port, video_filename))
            server_thread.start()

            try:
                while True:  # Keep the main thread alive until interrupted
                    pass
            except KeyboardInterrupt:
                pass
        else:
            print("No video filename provided in the RTSP URL.")

    rtsp_url = f"rtsp://localhost:{rtsp_port}/?filename=path_to_your_video_file.mp4"
    print(f"RTSP URL: {rtsp_url}")

    handle_rtsp_request(rtsp_url)
