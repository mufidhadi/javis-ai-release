sudo apt update
# Install MySQL Server
sudo apt install -y mysql-server

# Start and enable MySQL service
sudo systemctl start mysql
sudo systemctl enable mysql

# Secure MySQL installation (set root password and remove anonymous users)
sudo mysql_secure_installation

sudo mysql<<MYSQL_SCRIPT
SET GLOBAL validate_password.LENGTH = 4;
SET GLOBAL validate_password.policy = 0;
SET GLOBAL validate_password.mixed_case_count = 0;
SET GLOBAL validate_password.number_count = 0;
SET GLOBAL validate_password.special_char_count = 0;
SET GLOBAL validate_password.check_user_name = 0;
FLUSH PRIVILEGES;
MYSQL_SCRIPT

# Create a MySQL user 'ai' with password 'JTAlbarokah313', grant all privileges, and allow remote connections
sudo mysql<<MYSQL_SCRIPT
CREATE USER 'ai'@'%' IDENTIFIED WITH mysql_native_password BY 'JTAlbarokah313';
GRANT ALL PRIVILEGES ON *.* TO 'ai'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;
MYSQL_SCRIPT

# rtsp server
cd yolov8-tracking

# setup database
sudo mysql < release_db.sql
