#!/bin/bash

echo "main code setup"

# # install PM2
# sudo npm install -g pm2
# # sudo env PATH=$PATH:/usr/bin /usr/lib/node_modules/pm2/bin/pm2 startup systemd -u ai --hp /home/ai
# sudo env PATH=$PATH:/usr/bin /usr/lib/node_modules/pm2/bin/pm2 startup systemd -u ai --hp /home

# PM2 manager
cd pm2_manager
npm install
pm2 start index.js --name pm2_mgr --watch
pm2 save

cd ..

# rtsp MJPEG
cd rtsp_mjpeg
pm2 start main.py --interpreter=python3 --name mjpeg
pm2 save

cd ..

# rtsp server
chmod +x mediamtx
pm2 start mediamtx --name rtsp
pm2 save

# setup storage manager
cd yolov8-tracking
pm2 start storage_manager.py --interpreter=python3 --name=storage-mgr --cron-restart="0 0 * * *"
pm2 save

# setup all ai services
# pm2 start main.py --cron-restart="0 0 * * *" --interpreter=python3 --name cam1 -- -i 1
# pm2 start main.py --cron-restart="0 0 * * *" --interpreter=python3 --name cam2 -- -i 2
pm2 start main.py --cron-restart="0 0 * * *" --interpreter=python3 --name cam3 -- -i 3
# pm2 start main.py --cron-restart="0 0 * * *" --interpreter=python3 --name cam4 -- -i 4
# pm2 start main.py --cron-restart="0 0 * * *" --interpreter=python3 --name cam5 -- -i 5
pm2 start main.py --cron-restart="0 0 * * *" --interpreter=python3 --name cam6 -- -i 6
# pm2 start main.py --cron-restart="0 0 * * *" --interpreter=python3 --name cam7 -- -i 7
# pm2 start main.py --cron-restart="0 0 * * *" --interpreter=python3 --name cam8 -- -i 8

cd ..

# UI backend
cd streamer_ui/backend/
pm2 start index.js --name=ui_backend --watch
pm2 save

cd ..
cd ..

# UI frontend
cd streamer_ui/frontend/
pm2 serve build/ 3000 --name ui_frontend --spa --watch
pm2 save

cd ..
cd ..

echo "done code setup"

echo "Press [CTRL+C] to stop.."
while true
do
    # echo "."
	sleep 1
done