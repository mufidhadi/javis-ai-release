const express = require('express');
const bodyParser = require('body-parser');
const authRouter = require('./routes/auth');
const cors = require('cors');
const authenticateToken = require('./middleware/auth');
const { db } = require('./db');

const port = process.env.PORT || 3334;
const app = express();

// app.use(cors()); // Enable CORS for all routes
// app.options('*', cors())
// enable cors
app.use(
    cors({
        origin: true,
        optionsSuccessStatus: 200,
        credentials: true,
    })
);
app.options(
    '*',
    cors({
        origin: true,
        optionsSuccessStatus: 200,
        credentials: true,
        // allowedHeaders:'x-mufid'
    })
);



app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/auth', authRouter);

// Protected route example
app.get('/protected', authenticateToken, (req, res) => {
    res.json({ message: 'This is a protected route', user: req.user });
});

// Define API routes
// Create a new record
app.post('/kamera', (req, res) => {
    const { nama, ip, lokasi, rtsp, id_dashboard } = req.body;
    const insertQuery = 'INSERT INTO kamera (nama, ip, lokasi, rtsp, id_dashboard) VALUES (?, ?, ?, ?, ?)';
    db.query(insertQuery, [nama, ip, lokasi, rtsp, id_dashboard], (err, result) => {
        if (err) {
            console.error('Error creating record:', err);
            res.status(500).json({ error: 'Error creating record' });
            return;
        }
        res.status(201).json({ message: 'Record created successfully', id: result.insertId });
    });
});

// Read all records
app.get('/kamera', (req, res) => {
    const selectQuery = 'SELECT id,nama,ip,lokasi,rtsp,id_dashboard FROM kamera';
    db.query(selectQuery, (err, rows) => {
        if (err) {
            console.error('Error fetching records:', err);
            res.status(500).json({ error: 'Error fetching records' });
            return;
        }
        res.json(rows);
    });
});

app.get('/kamera/:id/line', (req, res) => {
    const id = req.params.id;
    const selectQuery = 'SELECT garis1_start, garis1_end, garis2_start, garis2_end FROM kamera WHERE id = ?';
    db.query(selectQuery, [id], (err, rows) => {
        if (err) {
            console.error('Error fetching record:', err);
            res.status(500).json({ error: 'Error fetching record' });
            return;
        }
        if (rows.length === 0) {
            res.status(404).json({ message: 'Record not found' });
        } else {
            res.json(rows[0]);
        }
    });
});

app.get('/kamera/:id/line/update', (req, res) => {
    const id = req.params.id;
    const query = req.query
    const queryKeys = Object.keys(query)
    let mysqlQuery = ''
    mysqlQuery = 'UPDATE kamera SET'
    queryKeys.map((key,i)=>{
        mysqlQuery += ' '+key+'="'+query[key]+'"'
        if (i < (queryKeys.length-1)) {
            mysqlQuery += ','
        }
    })
    mysqlQuery += ' WHERE id='+id
    res.send(mysqlQuery)
    // db.query(mysqlQuery,(err,result)=>{
    //     if (err) {
    //         console.error('Error updating record:', err);
    //         res.status(500).json({ error: 'Error updating record' });
    //         return;
    //     }
    //     console.log(result)
    // })
});

// Read a single record by ID
app.get('/kamera/:id', (req, res) => {
    const id = req.params.id;
    const selectQuery = 'SELECT * FROM kamera WHERE id = ?';
    db.query(selectQuery, [id], (err, rows) => {
        if (err) {
            console.error('Error fetching record:', err);
            res.status(500).json({ error: 'Error fetching record' });
            return;
        }
        if (rows.length === 0) {
            res.status(404).json({ message: 'Record not found' });
        } else {
            res.json(rows[0]);
        }
    });
});

// Update a record by ID
app.put('/kamera/:id', (req, res) => {
    const id = req.params.id;
    const { nama, ip, lokasi, rtsp, id_dashboard } = req.body;
    const updateQuery = 'UPDATE kamera SET nama = ?, ip = ?, lokasi = ?, rtsp = ?, id_dashboard = ? WHERE id = ?';
    db.query(updateQuery, [nama, ip, lokasi, rtsp, id_dashboard, id], (err, result) => {
        if (err) {
            console.error('Error updating record:', err);
            res.status(500).json({ error: 'Error updating record' });
            return;
        }
        if (result.affectedRows === 0) {
            res.status(404).json({ message: 'Record not found' });
        } else {
            res.json({ message: 'Record updated successfully' });
        }
    });
});

// Delete a record by ID
app.delete('/kamera/:id', (req, res) => {
    const id = req.params.id;
    const deleteQuery = 'DELETE FROM kamera WHERE id = ?';
    db.query(deleteQuery, [id], (err, result) => {
        if (err) {
            console.error('Error deleting record:', err);
            res.status(500).json({ error: 'Error deleting record' });
            return;
        }
        if (result.affectedRows === 0) {
            res.status(404).json({ message: 'Record not found' });
        } else {
            res.json({ message: 'Record deleted successfully' });
        }
    });
});

// Read all records
app.get('/vid_log', (req, res) => {
    const selectQuery = 'SELECT * FROM vid_log';
    db.query(selectQuery, (err, rows) => {
        if (err) {
            console.error('Error fetching records:', err);
            res.status(500).json({ error: 'Error fetching records' });
            return;
        }
        res.json(rows);
    });
});

app.get('/kamera/:id/vid_log', (req, res) => {
    const id = req.params.id
    const selectQuery = 'SELECT * FROM vid_log where id_kamera = ?';
    db.query(selectQuery,[id], (err, rows) => {
        if (err) {
            console.error('Error fetching records:', err);
            res.status(500).json({ error: 'Error fetching records' });
            return;
        }
        res.json(rows);
    });
});

app.get('/kamera/:id/vid_log/:interval_min?/:hour_lim?', (req, res) => {
    const id = req.params.id
    const h_limit = (req.params.hour_lim!=null)? req.params.hour_lim : 1
    const i_min = (req.params.interval_min!=null)? req.params.interval_min : 0
    const interval = (i_min==0)? 1 : i_min * 60

    // const selectQuery = 'select * from vid_log where id_kamera=? time > (select time from vid_log where id_kamera=? order by id desc limit 1) - interval 1 hour;';
    const selectQuery = 'SELECT   FLOOR(UNIX_TIMESTAMP(time)/(?)) AS timekey, id, id_kamera, sum(person) as person, sum(car) as car, sum(motorcycle) as motorcycle, sum(bus) as bus, sum(truck) as truck, time FROM vid_log where time > (select time from vid_log where id_kamera=? order by id desc limit 1) - interval ? hour and id_kamera=? GROUP BY timekey order by id asc';
    db.query(selectQuery,[interval,id,h_limit,id], (err, rows) => {
        if (err) {
            console.error('Error fetching records:', err);
            res.status(500).json({ error: 'Error fetching records' });
            return;
        }
        res.json(rows);
    });
});

app.get('/kamera/:id/vid_log/last_hour', (req, res) => {
    const selectQuery = 'select * from vid_log where id_kamera=? time > (select time from vid_log where id_kamera=? order by id desc limit 1) - interval 1 hour;';
    const id = req.params.id
    db.query(selectQuery,[id,id], (err, rows) => {
        if (err) {
            console.error('Error fetching records:', err);
            res.status(500).json({ error: 'Error fetching records' });
            return;
        }
        res.json(rows);
    });
});


// Start the server
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
