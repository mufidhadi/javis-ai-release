// routes/auth.js
const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const router = express.Router();
const { db } = require('../db');

// Replace with your user data store (e.g., database)
const users = [
    {
        id: 1,
        username: 'exampleuser',
        password: '$2b$10$U2UujAwRWgM1iRAioJrqHez/ZNdx2IAVOhPEOLdRUOfnMQyMoaaEq' // Hashed password
    }
];

// Secret key for JWT
const JWT_SECRET_KEY = 'your_secret_key';

router.post('/login', async (req, res) => {
    const { username, password } = req.body;
    console.log('le body ~',req.body);

    // Find the user by username (replace with database query)
    const selectQuery = 'SELECT * FROM users WHERE username = ?';
    db.query(selectQuery, [username], (err, rows) => {
        if (err) {
            console.error('Error fetching record:', err);
            res.status(500).json({ error: 'Error fetching record' });
            return;
        }
        if (rows.length === 0) {
            res.status(404).json({ message: 'User not found' });
        } else {
            // const user = users.find((u) => u.username === username);
            // const user = await db.query('SELECT * FROM users WHERE username = ?', [username]);
            const user = rows[0]
            console.log(user);
        
            if (!user) {
                return res.status(401).json({ message: 'User not found' });
            }
        
            // Compare the provided password with the stored hashed password
            bcrypt.compare(password, user.password, (err, result) => {
                if (err || !result) {
                    return res.status(401).json({ message: 'Invalid password' });
                }
        
                // Generate a JWT token
                const token = jwt.sign({ id: user.id, username: user.username }, 'your_secret_key', {
                    expiresIn: '1h' // Token expiration time
                });
        
                res.json({ token });
            });
        }
    });

});

router.post('/register', async (req, res) => {
    try {
        const { username, password } = req.body;
        console.log('le body ~',req.body);

        // Check if the username already exists
        const existingUser = await db.query('SELECT * FROM users WHERE username = ?', [username]);

        if (existingUser.length > 0) {
            return res.status(400).json({ message: 'Username already exists' });
        }

        // Hash the password
        const saltRounds = 10;
        const hashedPassword = await bcrypt.hash(password, saltRounds);

        // Insert the new user into the database
        await db.query('INSERT INTO users (username, password) VALUES (?, ?)', [username, hashedPassword]);

        // Generate a JWT token for the registered user
        const token = jwt.sign({ username }, 'your_secret_key', {
            expiresIn: '1h'
        });

        res.json({ token });
    } catch (error) {
        console.error('Registration error:', error);
        res.status(500).json({ message: 'Registration failed' });
    }
});

router.post('/tes',(req,res)=>{
    res.json(req.body)
})

module.exports = router;
