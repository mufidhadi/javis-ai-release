import React, { useEffect, useState } from "react"
import AdminLte from "../../components/admin_lte"
import AdminLteCard from "../../components/admin_lte_card"
import axios from "axios"
import { TabelBootstrap } from "../../components/TabelBootstrap"
import { Link, useLocation, useNavigate } from "react-router-dom"
import { TombolWatch } from "../../components/tombol_watch"
import { byteKeUkuranData, detikKeWaktu } from "../../utils/konversi"
import { toast } from "react-toastify"

export default function AutorunList({ }) {
    const navigate = useNavigate()
    const location = useLocation()
    const [data, setData] = useState([])
    // const [ws, setWs] = useState(null); // WebSocket connection
    let ws = null
    let closing_pindah_halaman = false

    useEffect(() => {
        // httpGetPm2List()
        setupWebSocket()

        // Clean up WebSocket connection when component unmounts
        return () => {
            console.log('💀💀💀💀💀💀💀');
            if (ws!==null) {
                console.log('Cleaning up WebSocket connection');
                closing_pindah_halaman = true
                ws.close();
            }
        }
    }, [])
    // }, [data])

    useEffect(()=>{
        console.log(location);
        if (location.pathname!='/autorun') {
            console.log('/autorun');
            if(ws){
                console.log('Closing WebSocket connection due to route change')
                ws.close()
            }
            return ()=>{
            }
        }
    },[location.pathname])

    function setupWebSocket() {
        // const ws = new WebSocket('ws://'+window.location.hostname+':5556/')
        ws = new WebSocket('ws://'+window.location.hostname+':5556/')

        ws.onopen = () => {
            console.log('WebSocket connection opened');
        };

        ws.onmessage = (event) => {
            const dataFromServer = JSON.parse(event.data);
            console.log('Received data from WebSocket:', dataFromServer);

            // Update the data when WebSocket data is received
            if (dataFromServer.processes) {
                const pm2Data = dataFromServer;
                updateData(pm2Data);
            }
        };

        ws.onclose = () => {
            console.log('WebSocket connection closed');
            if (!closing_pindah_halaman)
            setupWebSocket()
        };

        ws.onerror = (error) => {
            console.error('WebSocket error:', error);
        };

        // setWs(ws)
    }

    function wsGetPm2List() {

    }

    function httpGetPm2List() {
        const token = sessionStorage.getItem('token'); // Retrieve the token from storage
        // console.log('🖥️',token);

        if (!token) {
            // Handle not authenticated
            return;
        }

        // Make an authenticated request to the secured route
        axios.get('http://'+window.location.hostname+':5555/list')
            .then((response) => {
                if (response.data.processes) {
                    const response_data = response.data
                    updateData(response_data)
                }
                // console.log('🖥️ response',response.data.processes);
            })
            .catch((error) => {
                console.error('Error fetching data:', error);
            });
    }

    function updateData(pm2_data) {
        let newData = []
        pm2_data.processes.map((pm2_process, i) => {
            newData.push({
                id: pm2_process.pm_id,
                name: pm2_process.name,
                id_kamera: (pm2_process.name.includes('cam')) ? pm2_process.name.replace('cam', '') : '',
                status: pm2_process.pm2_env.status,
                resource: {
                    cpu: pm2_process.monit.cpu,
                    // memory:pm2_process.monit.memory,
                    memory: byteKeUkuranData(pm2_process.monit.memory),
                },
                // uptime:pm2_process.pm2_env.pm_uptime,
                // uptime:detikKeWaktu(pm2_process.pm2_env.pm_uptime),
                jumlah_restart: pm2_process.pm2_env.restart_time,
                action: [
                    <>
                        <button onClick={() => pm2Restart(pm2_process.name)} className="btn btn-default">
                            <i className="fas fa-sync" />
                            &ensp;
                            Restart
                        </button>
                        &ensp;
                        <button onClick={() => pm2Stop(pm2_process.name)} className="btn btn-danger">
                            <i className="fas fa-stop" />
                            &ensp;
                            Stop
                        </button>
                        &ensp;
                        <button onClick={() => pm2Delete(pm2_process.name)} className="btn btn-danger" disabled={!pm2_process.name.includes('cam')}>
                            <i className="fas fa-trash" />
                            &ensp;
                            Delete
                        </button>
                    </>
                ]
            })
        })
        setData(newData)
    }

    function pm2Restart(pm2_id) {
        console.log('pm2Restart')

        axios.get('http://'+window.location.hostname+':5555/restart/' + pm2_id)
            .then((response) => {
                toast(response)
            })
            .catch((error) => {
                console.error('Error fetching data:', error)
            })
    }

    function pm2Stop(pm2_id) {
        console.log('pm2Stop')

        axios.get('http://'+window.location.hostname+':5555/stop/' + pm2_id)
            .then((response) => {
                toast(response)
            })
            .catch((error) => {
                console.error('Error fetching data:', error)
            })
    }

    function pm2Delete(pm2_id) {
        console.log('pm2Delete')

        axios.get('http://'+window.location.hostname+':5555/delete/' + pm2_id)
            .then((response) => {
                toast(response)
            })
            .catch((error) => {
                console.error('Error fetching data:', error)
            })
    }

    return (
        <>
            <AdminLte judul='Autorun'>
                <div className='container-fluid'>
                    <div className='row'>
                        <div className='col-12'>
                            <AdminLteCard
                                tittle='List'
                                header_component={
                                    <>
                                        {/* <Link to='./add' relative="path" className="btn btn-primary">
                                            <i className="fas fa-plus"></i>
                                            &ensp;
                                            Tambah
                                        </Link> */}
                                    </>
                                }
                            >
                                <TabelBootstrap data={data} show_key={false} use_standard_action={false} />
                            </AdminLteCard>
                        </div>
                    </div>
                </div>
            </AdminLte>
        </>
    )
}