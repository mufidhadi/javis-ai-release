import React, { useEffect, useState } from "react"
import { Link, useNavigate, useParams } from "react-router-dom"
import AdminLte from "../../components/admin_lte"
import AdminLteCard from "../../components/admin_lte_card"
import axios from "axios"
import { DetailBasicBootstrap } from "../../components/detail_basic_bootstrap"
import { DetailCardBootstrap } from "../../components/detail_card_bootstrap"
import { TombolKembali } from "../../components/tombol_kembali";

export default function DetailKamera({ }) {
    const navigate = useNavigate()
    const { id } = useParams()

    const [data, setData] = useState({})

    useEffect(() => {
        const token = sessionStorage.getItem('token')

        axios.get('http://'+window.location.hostname+':3334/kamera/' + id, {
            headers: {
                Authorization: token
            }
        })
            .then((response) => {
                setData(response.data)
            })
            .catch((error) => {
                console.error('Error fetching data:', error)
            })
    }, [])

    return (
        <>
            <AdminLte judul="Detail Kamera">
                <div className="conatiner-fluid">
                    <div className="row">
                        <div className="col-12">
                            <AdminLteCard
                                tittle="Kamera"
                                header_component={
                                    <>
                                        <TombolKembali />
                                        &ensp;
                                        <TombolEdit id={id} />
                                    </>
                                }
                                footer_component={
                                    <>
                                        <TombolKembali />
                                    </>
                                }
                            >
                                {/* <pre>{JSON.stringify(data)}</pre> */}
                                {/* <DetailBasicBootstrap data={data} /> */}
                                <DetailCardBootstrap data={data} />
                            </AdminLteCard>
                        </div>
                    </div>
                </div>
            </AdminLte>
        </>
    )
}

function TombolEdit({id}) {
    return (
        <>
            <Link to={'/kamera/edit/'+id} className="btn btn-default">
                <i className="fas fa-edit" />
                &ensp;
                Edit
            </Link>
        </>
    )
}