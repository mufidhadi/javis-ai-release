import React, { useEffect, useState } from "react";
import AdminLte from "../../components/admin_lte";
import AdminLteCard from "../../components/admin_lte_card";
import { FormBootstrap } from "../../components/form_bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import { toast } from "react-toastify";
import { TombolKembali } from "../../components/tombol_kembali";

export default function FormKamera({ tambah_or_edit = 'tambah' }) {
    const navigate = useNavigate()
    const { id } = useParams()

    const [isian, setIsian] = useState({})
    const [pancingan, setPancingan] = useState('')
    const [inputList, setInputList] = useState([])

    let input_list_template = [
        {
            type: 'hidden',
            // label: 'id',
            name: 'id',
            id: 'id',
            value:null,
        },
        {
            type: 'text',
            label: 'nama kamera',
            name: 'nama',
            id: 'nama',
            value:null,
            help_text: 'berikan nama pada kamera ini (misal: gerbang 1)',
        },
        {
            type: 'text',
            label: 'ip kamera',
            name: 'ip',
            id: 'ip',
            value:null,
            help_text: 'isikan alamat ip kamera ini',
        },
        {
            type: 'text',
            label: 'lokasi kamera',
            name: 'lokasi',
            id: 'lokasi',
            value:null,
            help_text: 'isikan lokasi fisik kamera ini (singkat, misal: gerbang depan)',
        },
        {
            type: 'text',
            label: 'alamat rtsp kamera',
            name: 'rtsp',
            id: 'rtsp',
            value:null,
            help_text: 'isikan alamat rtsp untuk kamera ini',
        },
        {
            type: 'number',
            label: 'id_dashboard kamera',
            name: 'id_dashboard',
            id: 'id_dashboard',
            value:null,
            help_text: 'isikan id dashboard kamera ini sesuai di dashboard web utama',
        },
    ]

    let submit_url = 'http://'+window.location.hostname+':3334/kamera'

    useEffect(() => {
        setInputList(input_list_template)
        if (tambah_or_edit == 'edit') {
            submit_url += '/' + id

            const token = sessionStorage.getItem('token'); // Retrieve the token from storage
            // console.log('🖥️',token);

            if (!token) {
                // Handle not authenticated
                return;
            }

            // Make an authenticated request to the secured route
            axios.get('http://'+window.location.hostname+':3334/kamera/' + id, {
                headers: {
                    Authorization: token // Include the JWT token in the headers
                }
            })
                .then((response) => {
                    setIsian(response.data)
                    input_list_template.map((inpt, x) => {
                        input_list_template[x].value = response.data[inpt.name]
                    })
                    setInputList(input_list_template)
                    setPancingan(JSON.stringify(response.data))
                    // console.log(input_list_template,response.data)
                })
                .catch((error) => {
                    console.error('Error fetching data:', error);
                });

        }
    }, [])

    const onSubmitHandler = async (e) => {
        e.preventDefault()
        // console.log('✅ SUBMIT', isian)
        try {
            let response
            if (tambah_or_edit == 'edit') {
                submit_url += '/' + id
                response = await axios.put(submit_url, isian)
            }else{
                response = await axios.post(submit_url, isian)
            }
            if (response.data.message) {
                toast(response.data.message)
                // navigate('/kamera')
                navigate(-1)
            }
            console.log(response)
        } catch (error) {
            console.log('🪲 ERROR CUY:', error)
        }
    }

    for (let i = 0; i < input_list_template.length; i++) {
        const input = input_list_template[i];
        // isian[input.name] = null
        input_list_template[i] = {
            ...input, onChangeHandler: (e) => {
                setIsian(isiLama=>{
                    return {...isiLama, [e.target.name]:e.target.value}
                })
                setInputList(inputListLama=>{
                    let inputListBaru = []
                    inputListLama.map((il,i)=>{
                        const il_temp = il
                        if (il.name==e.target.name) {
                            il_temp.value = e.target.value
                        }
                        inputListBaru.push(il_temp)
                    })
                    return inputListBaru
                })
                setPancingan(e.target.value)
            }
        }
    }
    // console.log(input_list);

    return (
        <>
            <AdminLte judul='Kamera'>
                <div className='container-fluid'>
                    <div className='row'>
                        <div className='col-12'>
                            <AdminLteCard
                                tittle={'Form ' + tambah_or_edit + ' Kamera'}
                                header_component={
                                    <>
                                        <TombolKembali />
                                    </>
                                }
                                footer_component={
                                    <>
                                        <TombolKembali />
                                    </>
                                }
                            >
                                <FormBootstrap onSubmit={onSubmitHandler} input_list={inputList} />
                            </AdminLteCard>
                        </div>
                    </div>
                </div>
            </AdminLte>
        </>
    )
}

