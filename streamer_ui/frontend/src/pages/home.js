import React, { useEffect, useState } from "react"
import AdminLte from "../components/admin_lte"
import AdminLteCard from "../components/admin_lte_card"
import { SmallBox } from "../components/info_box"
import axios from "axios"
import { BasicChart, DonutChart, LineChart, PieChart } from "../components/chart"
import { sortDataByName } from "../utils/sorter"
import { byteKeUkuranData } from "../utils/konversi"
import { DetailBasicBootstrap } from "../components/detail_basic_bootstrap"
import { TabelBootstrap } from "../components/TabelBootstrap"
import { TombolWatch } from "../components/tombol_watch"
import { Chart } from "react-chartjs-2"

export default function Home() {
    const [jumlahKamera, setJumlahKamera] = useState(0)
    const [jumlahAutorun, setJumlahAutorun] = useState(0)
    const [jumlahAI, setJumlahAI] = useState(0)
    const [memData, setMemData] = useState([])
    const [autorunData, setAutorunData] = useState([])
    const [memDataFull, setMemDataFull] = useState({})
    const [AICamData, setAICamData] = useState([])
    const [aiIds, setAiIds] = useState([])

    const [AIChartData, setAIChartData] = useState([])

    let ws = null
    let closing_pindah_halaman = false

    useEffect(() => {
        const token = sessionStorage.getItem('token'); // Retrieve the token from storage

        if (!token) {
            // Handle not authenticated
            return;
        }

        // Make an authenticated request to the secured route
        updateJumlahKamera(token)
        updateJumlahAutorun(token)
        closing_pindah_halaman = false
        setupWebSocket()

        return () => {
            console.log('💀💀💀💀💀💀💀');
            if (ws !== null) {
                console.log('Cleaning up WebSocket connection');
                closing_pindah_halaman = true
                ws.close();
            }
        }
    }, [])

    return (
        <>
            <AdminLte judul='Home'>
                <div className='container-fluid'>
                    <div className="row">
                        <div className="col-sm-6 col-md-4">
                            <SmallBox fa_icon="fa-camera" href="/kamera" title="Kamera CCTV" number={jumlahKamera} />
                        </div>
                        <div className="col-sm-6 col-md-4">
                            <SmallBox fa_icon="fa-play" href="/autorun" title="autorun process" number={jumlahAutorun} />
                        </div>
                        <div className="col-sm-6 col-md-4">
                            <SmallBox fa_icon="fa-microchip" href="/ai" title="AI service" number={jumlahAI} />
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-6">
                            <AdminLteCard tittle="autorun sistem penunjang AI">
                                {/* {JSON.stringify(autorunData)} */}
                                <TabelBootstrap data={sortDataByName(autorunData)} show_key={false} use_standard_action={false} />
                            </AdminLteCard>
                        </div>
                        <div className="col-md-6">
                            <AdminLteCard tittle="penggunaan memory sistem (byte)">
                                {/* {JSON.stringify(memData)} */}
                                <BasicChart type="bar" data={memData} data_label="name" data_value="memory_usage" />
                            </AdminLteCard>
                        </div>
                    </div>

                    <div className="row">
                        {AICamData.map((AICam, num) => (
                            <>
                                <div className="col-md-6" key={num}>
                                    <AdminLteCard tittle={"jumlah deteksi AI cam" + AICam.id_kamera + " (realtime 30 detik)"}>
                                        {/* {JSON.stringify(AICam)} */}
                                        <h4>SMP realtime: {AICam.smp}</h4>
                                        <BasicChart type="bar" data={AICam.objek} data_label="name" data_value="count" />
                                        <p>
                                            {/* <TombolWatch id={AICam.id_kamera} /> */}
                                        </p>
                                    </AdminLteCard>
                                </div>
                                <div className="col-md-6" key={num + 'chart'}>
                                    <AdminLteCard tittle={"jumlah deteksi AI cam" + AICam.id_kamera + ""}>
                                        <Chart type="line" data={{
                                            labels: AIChartData[num].labels,
                                            datasets: AIChartData[num].datasets
                                        }} options={{
                                            plugins: {
                                                legend: {
                                                    display: false
                                                }
                                            }
                                        }} />
                                    </AdminLteCard>
                                </div>
                            </>
                        ))}
                    </div>

                    {/* <div className="row">
                        <div className="col-md-6">
                            <AdminLteCard tittle="chart">
                                <BasicChart type="scatter" />
                            </AdminLteCard>
                        </div>
                        <div className="col-md-6">
                            <AdminLteCard tittle="chart">
                                <BasicChart type="line" />
                            </AdminLteCard>
                        </div>
                        <div className="col-md-6">
                            <AdminLteCard tittle="chart">
                                <BasicChart type="bar" />
                            </AdminLteCard>
                        </div>
                        <div className="col-md-6">
                            <AdminLteCard tittle="chart">
                                <BasicChart type="radar" />
                            </AdminLteCard>
                        </div>
                        <div className="col-md-6">
                            <AdminLteCard tittle="chart">
                                <BasicChart type="pie" />
                            </AdminLteCard>
                        </div>
                        <div className="col-md-6">
                            <AdminLteCard tittle="chart">
                                <BasicChart type="doughnut" />
                            </AdminLteCard>
                        </div>
                        <div className="col-md-6">
                            <AdminLteCard tittle="chart">
                                <BasicChart type="polarArea" />
                            </AdminLteCard>
                        </div>
                    </div> */}

                    {/* <div className='row'>
                        <div className='col-12'>
                            <AdminLteCard tittle='Home'>
                                <h1>Hello, world!</h1>
                            </AdminLteCard>
                        </div>
                    </div> */}
                </div>
            </AdminLte>
        </>
    )

    function updateJumlahKamera(token) {
        axios.get('http://' + window.location.hostname + ':3334/kamera', {
            headers: {
                Authorization: token // Include the JWT token in the headers
            }
        })
            .then((response) => {
                setJumlahKamera(response.data.length)
            })
            .catch((error) => {
                console.error('Error fetching data:', error)
            })
    }

    function updateJumlahAutorun(token) {
        axios.get('http://' + window.location.hostname + ':5555/list', {
            headers: {
                Authorization: token // Include the JWT token in the headers
            }
        })
            .then((response) => {
                setJumlahAutorun(response.data.processes.length)
                let jumlah_autorun_ai = 0
                response.data.processes.map((pm2) => {
                    if (pm2.name.includes('cam')) {
                        jumlah_autorun_ai += 1
                    }
                })
                setJumlahAI(jumlah_autorun_ai)
            })
            .catch((error) => {
                console.error('Error fetching data:', error)
            })
    }

    function setupWebSocket() {
        // const ws = new WebSocket('ws://'+window.location.hostname+':5556/')
        ws = new WebSocket('ws://' + window.location.hostname + ':5556/')

        ws.onopen = () => {
            console.log('WebSocket connection opened');
        };

        ws.onmessage = (event) => {
            const dataFromServer = JSON.parse(event.data);
            // console.log('Received data from WebSocket:', dataFromServer);

            // Update the data when WebSocket data is received
            if (dataFromServer.processes) {
                const pm2Data = dataFromServer;
                updateAutorunData(pm2Data)
            }

            if (dataFromServer.memory_usage) {
                // setMemData(dataFromServer.memory_usage)
                setMemData(sortDataByName(dataFromServer.memory_usage))
            }

            if (dataFromServer.id_kamera) {
                // console.log(dataFromServer)
                if (aiIds.indexOf(dataFromServer.id_kamera) == -1) {
                    let newAICamData = AICamData
                    newAICamData.push(dataFromServer)
                    setAICamData(newAICamData)
                    let newAiIds = aiIds
                    aiIds.push(dataFromServer.id_kamera)
                    setAiIds(newAiIds)

                    const waktu = new Date(dataFromServer.waktu * 1000)
                    let waktuStr = waktu.toLocaleDateString() + ' ' + waktu.toLocaleTimeString()


                    let newAIChartData = AIChartData
                    newAIChartData.push({
                        id_kamera: dataFromServer.id_kamera,
                        labels: [waktuStr],
                        datasets: [
                            {
                                label: 'person',
                                data: [dataFromServer.person],
                                borderColor: '#7149C6'
                            },
                            {
                                label: 'car',
                                data: [dataFromServer.car],
                                borderColor: '#FC2947'
                            },
                            {
                                label: 'motorcycle',
                                data: [dataFromServer.motorcycle],
                                borderColor: '#F5EA5A'
                            },
                            {
                                label: 'bus',
                                data: [dataFromServer.bus],
                                borderColor: '#54B435'
                            },
                            {
                                label: 'truck',
                                data: [dataFromServer.truck],
                                borderColor: '#D800A6'
                            },
                            {
                                label: 'smp',
                                data: [dataFromServer.smp],
                                borderColor: '#3B0000'
                            },
                        ]
                    })
                    setAIChartData(newAIChartData)
                } else {
                    let idx = AICamData.findIndex(obj => obj.id_kamera == dataFromServer.id_kamera)
                    let newAICamData = AICamData
                    newAICamData[idx] = dataFromServer
                    setAICamData(newAICamData)

                    const waktu = new Date(dataFromServer.waktu * 1000)
                    let waktuStr = waktu.toLocaleDateString() + ' ' + waktu.toLocaleTimeString()

                    let idxChart = AIChartData.findIndex(obj => obj.id_kamera == dataFromServer.id_kamera)
                    let newAIChartData = AIChartData

                    limitArray(newAIChartData[idxChart].labels)
                    newAIChartData[idxChart].labels.push(waktuStr)
                    // person
                    limitArray(newAIChartData[idxChart].datasets[0].data)
                    newAIChartData[idxChart].datasets[0].data.push(dataFromServer.person)
                    // car
                    limitArray(newAIChartData[idxChart].datasets[1].data)
                    newAIChartData[idxChart].datasets[1].data.push(dataFromServer.car)
                    // motorcycle
                    limitArray(newAIChartData[idxChart].datasets[2].data)
                    newAIChartData[idxChart].datasets[2].data.push(dataFromServer.motorcycle)
                    // bus
                    limitArray(newAIChartData[idxChart].datasets[3].data)
                    newAIChartData[idxChart].datasets[3].data.push(dataFromServer.bus)
                    // truck
                    limitArray(newAIChartData[idxChart].datasets[4].data)
                    newAIChartData[idxChart].datasets[4].data.push(dataFromServer.truck)
                    // smp
                    limitArray(newAIChartData[idxChart].datasets[5].data)
                    newAIChartData[idxChart].datasets[5].data.push(dataFromServer.smp)

                    setAIChartData(newAIChartData)
                }
            }
        };

        ws.onclose = () => {
            console.log('WebSocket connection closed');
            if (!closing_pindah_halaman)
                setupWebSocket()
        };

        ws.onerror = (error) => {
            console.error('WebSocket error:', error);
        };

        // setWs(ws)
    }

    function limitArray(array, limit = 10) {
        if (array.length == limit+1)
            array.shift()
        else if (array.length > limit)
            array = array.slice(array.length - limit, array.length)
        else
            array = array
        // console.log(array);
    }

    function updateAutorunData(pm2_data) {
        let newData = []
        pm2_data.processes.map((pm2_process, i) => {
            newData.push({
                id: pm2_process.pm_id,
                name: pm2_process.name,
                // id_kamera: (pm2_process.name.includes('cam')) ? pm2_process.name.replace('cam', '') : '',
                status: pm2_process.pm2_env.status,
                // cpu: pm2_process.monit.cpu,
                // memory:pm2_process.monit.memory,
                memory: byteKeUkuranData(pm2_process.monit.memory),
                // uptime:pm2_process.pm2_env.pm_uptime,
                // uptime:detikKeWaktu(pm2_process.pm2_env.pm_uptime),
                jumlah_restart: pm2_process.pm2_env.restart_time,
            })
        })
        setAutorunData(newData)
    }
}