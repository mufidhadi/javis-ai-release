import React, { useEffect, useState } from "react";
import AdminLte from "../../components/admin_lte";
import AdminLteCard from "../../components/admin_lte_card";
import { TombolKembali } from "../../components/tombol_kembali";
import { useParams } from "react-router-dom";
import { FormInputBootstrap } from "../../components/form_input_bootstrap";
import axios from "axios";
import { Chart } from "react-chartjs-2";
export default function AILog() {
    const { id } = useParams()
    const [chartInterval, setChartInterval] = useState(1)
    const [chartLimit, setChartLimit] = useState(1)
    const [chartDataRaw, setChartDataRaw] = useState([])
    const [chartData, setChartData] = useState({
        labels: [],
        datasets: [
            {
                label: 'person',
                data: [],
                borderColor: '#7149C6'
            },
            {
                label: 'car',
                data: [],
                borderColor: '#FC2947'
            },
            {
                label: 'motorcycle',
                data: [],
                borderColor: '#F5EA5A'
            },
            {
                label: 'bus',
                data: [],
                borderColor: '#54B435'
            },
            {
                label: 'truck',
                data: [],
                borderColor: '#D800A6'
            },
            {
                label: 'smp',
                data: [],
                borderColor: '#3B0000'
            },
        ]
    })
    const [ChartComponent, setChartComponent] = useState(<></>)

    useEffect(() => {
        const token = sessionStorage.getItem('token')
        if (!token) {
            // Handle not authenticated
            return;
        }
        axios.get('http://' + window.location.hostname + ':3334/kamera/' + id + '/vid_log/' + chartInterval + '/' + chartLimit + '/', {
            headers: {
                Authorization: token // Include the JWT token in the headers
            }
        })
            .then((response) => {
                // console.log(response.data)
                setChartDataRaw(response.data)
            })
            .catch((error) => {
                console.error('Error fetching data:', error);
            })
    }, [chartInterval, chartLimit])

    useEffect(() => {
        const newChartData = chartData
        newChartData.labels = []
        for (let i = 0; i < newChartData.datasets.length; i++) {
            const dataset = newChartData.datasets[i];
            newChartData.datasets[i].data = []
        }
        for (let i = 0; i < chartDataRaw.length; i++) {
            const rawData = chartDataRaw[i];
            const waktu = new Date(rawData.time.replace('T', ' ').replace('Z', ''))
            let waktuStr = waktu.toLocaleDateString() + ' ' + waktu.toLocaleTimeString()

            newChartData.labels.push(waktuStr)

            // person
            newChartData.datasets[0].data.push(rawData.person)
            // car
            newChartData.datasets[1].data.push(rawData.car)
            // motorcycle
            newChartData.datasets[2].data.push(rawData.motorcycle)
            // bus
            newChartData.datasets[3].data.push(rawData.bus)
            // truck
            newChartData.datasets[4].data.push(rawData.truck)
            // smp
            newChartData.datasets[5].data.push(rawData.smp)
        }

        setChartData(newChartData)
        console.log(newChartData)
        setChartComponent(<><Chart key={Math.random()} type="line" data={newChartData}/></>)
    }, [chartDataRaw])

    useEffect(()=>{
        console.log(chartData.labels.length)
    },[chartData])

    return (
        <>
            <AdminLte judul="Log Deteksi AI">
                <div className='container-fluid'>
                    <div className='row'>
                        <div className='col-12'>
                            <AdminLteCard tittle="Chart"
                                header_component={
                                    <>
                                        <TombolKembali />
                                    </>
                                }>
                                <div className="container">
                                    <div className="row" onChange={(e) => {
                                        // console.log(e.target.value, e.target.name)
                                        if (e.target.name === 'interval') {
                                            setChartInterval(e.target.value)
                                        }
                                        if (e.target.name === 'limit') {
                                            setChartLimit(e.target.value)
                                        }
                                    }}>
                                        <div className="col">
                                            <FormInputBootstrap key={'interval'} id={'interval'} label={'interval'} name={'interval'} type="select" options={[
                                                { label: '1 menit', value: 1 },
                                                { label: '10 menit', value: 10 },
                                                { label: '15 menit', value: 15 },
                                                { label: '30 menit', value: 30 },
                                                { label: '1 jam', value: 60 },
                                                { label: '12 jam', value: 12 * 60 },
                                                { label: '24 jam', value: 24 * 60 },
                                            ]} />
                                        </div>
                                        <div className="col">
                                            <FormInputBootstrap key={'limit'} id={'limit'} label={'limit'} name={'limit'} type="select" options={[
                                                { label: '1 jam terakhir', value: 1 },
                                                { label: '1 hari terakhir', value: 24 },
                                                { label: '1 minggu terakhir', value: 24 * 7 },
                                                { label: '30 hari terakhir', value: 24 * 30 },
                                            ]} />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col">
                                            {/* <Chart type="line" data={{chartData}} /> */}
                                            {ChartComponent}
                                            {/* interval: {chartInterval} <br /> */}
                                            {/* {JSON.stringify(chartData)} */}
                                            {/* {JSON.stringify(chartDataRaw)} */}
                                        </div>
                                    </div>
                                </div>
                            </AdminLteCard>
                        </div>
                    </div>
                </div>
            </AdminLte>
        </>
    )
}