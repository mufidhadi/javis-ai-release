export function detikKeWaktu(durasiDetik) {
    const detikPerHari = 86400; // 60 detik * 60 menit * 24 jam
    const detikPerJam = 3600; // 60 detik * 60 menit
    const detikPerMenit = 60;

    const hari = Math.floor(durasiDetik / detikPerHari);
    const sisaDetik1 = durasiDetik % detikPerHari;
    const jam = Math.floor(sisaDetik1 / detikPerJam);
    const sisaDetik2 = sisaDetik1 % detikPerJam;
    const menit = Math.floor(sisaDetik2 / detikPerMenit);
    const detik = sisaDetik2 % detikPerMenit;

    let hasil = "";

    if (hari > 0) {
        hasil += `${hari} hari `;
    }

    if (jam > 0) {
        hasil += `${jam} jam `;
    }

    if (menit > 0) {
        hasil += `${menit} menit `;
    }

    hasil += `${detik} detik`;

    return hasil.trim(); // Menghapus spasi ekstra di akhir string
}

export function byteKeUkuranData(byte) {
    if (byte === 0) return "0 Byte";

    const ukuranData = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    const pembagi = 1024;
    const digitDesimal = 2; // Angka desimal yang akan ditampilkan

    const ukuranDataIndex = Math.floor(Math.log(byte) / Math.log(pembagi));

    return parseFloat((byte / Math.pow(pembagi, ukuranDataIndex)).toFixed(digitDesimal)) + " " + ukuranData[ukuranDataIndex];
}