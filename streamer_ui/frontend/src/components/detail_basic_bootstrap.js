import React, { useEffect } from "react";

export function DetailBasicBootstrap({ data }) {
    useEffect(()=>{},[data])
    return (
        <>
            {(data !== {}) ?
                <table className="table">
                    <tbody>
                        {Object.keys(data).map((key) => <tr>
                            <th>{key}</th>
                            <td>{data[key]}</td>
                        </tr>
                        )}
                    </tbody>
                </table>
                :
                <></>
            }
        </>
    );
}
