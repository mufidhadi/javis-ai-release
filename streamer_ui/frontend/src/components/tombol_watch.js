import React from "react";
import { Link } from "react-router-dom";

export function TombolWatch({ id }) {
    return (
        <>
            <Link to={'./watch/' + id} relative="path" className="btn btn-default float-right">
                <i className="fas fa-tv" />
                &ensp;
                Lihat RTSP
            </Link>
        </>
    );
}
