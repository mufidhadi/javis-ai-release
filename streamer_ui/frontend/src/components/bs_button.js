import React from "react"
import { Link } from "react-router-dom"

export function BsButtonLink({ href = '/', text = 'Oke', fa_icon = 'fa-check', btn_type = 'default' }) {
    return (
        <>
            <Link to={href} relative="path" className={'btn btn-' + btn_type}>
                <i className={'fas ' + fa_icon}></i>
                &ensp;
                {text}
            </Link>
        </>
    )
}

export function BsButton({ clickHandler = () => { }, text = 'Oke', fa_icon = 'fa-check', btn_type = 'default' }) {
    return (
        <>
            <button onClick={clickHandler} className={'btn btn-' + btn_type}>
                <i className={'fas ' + fa_icon}></i>
                &ensp;
                {text}
            </button>
        </>
    )
}