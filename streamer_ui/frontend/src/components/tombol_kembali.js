import React from "react";
import { Link } from "react-router-dom";

export function TombolKembali() {
    return (
        <>
            <Link to={-1} className="btn btn-default">
                <i className="fas fa-chevron-circle-left" />
                &ensp;
                Kembali
            </Link>
        </>
    );
}
