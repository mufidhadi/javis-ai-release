import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"

export function DetailCardBootstrap({ data, level = 0, parent_key = '', max_col_size = 12, min_col_size = 3, child_col_size = 6, max_level = 2, parent_path = '', key_name = 'id' }) {
    const [colSizes, setColSizes] = useState([])
    useEffect(() => {
        let col_with_child_list = []
        let child_count = 0
        Object.keys(data).map((key, i) => {
            if (key !== key_name) {
                child_count++
                if (typeof data[key] === 'object') {
                    const divider = ((child_count - 1) > 1) ? (child_count - 1) : 1
                    let col_size = Math.round(12 / divider)
                    col_size = Math.min(max_col_size, col_size)
                    col_size = Math.max(min_col_size, col_size)
                    col_with_child_list.push({ id: i, count: child_count, divider, size: col_size, level })
                    child_count = 0
                }
            }
        })

        let temp_col_sizes = []
        let target_col_with_child_list_id = 0
        Object.keys(data).map((key, i) => {
            if (key !== key_name) {
                let temp_size = 12
                if (col_with_child_list.length > 0) {
                    if (i == col_with_child_list[target_col_with_child_list_id].id) {
                        if (target_col_with_child_list_id + 1 < col_with_child_list.length)
                            target_col_with_child_list_id++
                    }
                    temp_size = col_with_child_list[target_col_with_child_list_id].size
                }
                // temp_col_sizes.push({ i, temp_size, target_id: target_col_with_child_list_id })
                temp_col_sizes.push(temp_size)
            }
        })

        // console.log('level ' + level + ' 👉', col_with_child_list, temp_col_sizes)
        setColSizes(temp_col_sizes)

    }, [data])
    return (
        <>
            {data !== {} ?
                <>
                    <div className="row">
                        {Object.keys(data).map((key, i) =>
                            <>
                                {(key == key_name) ?
                                    <>
                                    </>
                                    :
                                    <Card key={i} colSizes={colSizes} i={i} current_key={key} data={data} parent_key={parent_key} parent_path={parent_path} level={level} max_level={max_level} child_col_size={child_col_size} min_col_size={min_col_size} key_name={key_name} />
                                }
                            </>
                        )}
                    </div>
                </>
                :
                <>
                    <div className="alert alert-warning" role="alert">
                        Maaf, belum ada data disini
                    </div>
                </>
            }
        </>
    )
}

function Card({ colSizes, i, current_key, data, parent_key, parent_path, level, max_level, child_col_size, min_col_size, key_name }) {
    return (
        <>
            <div className={"col-md-" + colSizes[i]}>
                <div className="card">
                    <div className="card-body">
                        <CardContent current_key={current_key} data={data} parent_key={parent_key} parent_path={parent_path} level={level} max_level={max_level} child_col_size={child_col_size} min_col_size={min_col_size} key_name={key_name} />
                    </div>
                </div>
            </div>
        </>
    )
}

function CardContent({ current_key, data, parent_key, parent_path, level, max_level, child_col_size, min_col_size, key_name }) {
    return (
        <>
            <div className="row">
                <CardTittle current_key={current_key} data={data} parent_key={parent_key} parent_path={parent_path} />
                <CardBody data={data} current_key={current_key} level={level} max_level={max_level} parent_path={parent_path} child_col_size={child_col_size} min_col_size={min_col_size} />
            </div>
        </>
    )
}

function CardTittle({ current_key, data, parent_key, parent_path }) {
    return (
        <>
            <div className="col-12">
                {(!isNaN(current_key)) ?
                    <>
                        {(typeof data[current_key] == 'object') ?
                            <h5 className="card-title mb-3">
                                {parent_key.replaceAll('_', ' ').toUpperCase()}
                                &ensp;
                                {parseInt(current_key) + 1}
                            </h5>
                            :
                            <></>
                        }
                    </>
                    :
                    <h5 className="card-title mb-3">
                        {current_key.replaceAll('_', ' ').toUpperCase()}
                    </h5>
                }
            </div>
        </>
    )
}

function CardBody({ data, current_key, level, max_level, parent_path, child_col_size, min_col_size, key_name }) {
    return <>
        <div className="col-12">
            {(typeof data[current_key] !== 'object') ?
                <p className="card-text">{data[current_key]}</p>
                :
                <>
                    {(level == max_level) ?
                        <p className="card-text">
                            <Link to={parent_path + '/' + current_key} relative="path" className="btn btn-default">
                                Lihat
                            </Link>
                        </p>
                        :
                        <>
                            <DetailCardBootstrap data={data[current_key]} level={level + 1} parent_key={current_key} max_col_size={child_col_size} min_col_size={min_col_size} key_name={key_name} parent_path={parent_path + '/' + current_key} max_level={max_level} />
                        </>
                    }
                </>
            }
        </div>
    </>
}

