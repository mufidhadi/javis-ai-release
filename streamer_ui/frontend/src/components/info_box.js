import React from "react"
import { Link } from "react-router-dom"

export function InfoBox({ title = '', number = 0, fa_icon = 'fa-info' }) {
    return (
        <>
            <div className="info-box bg-gradient-warning">
                <span className="info-box-icon"><i className={'fas ' + fa_icon}></i></span>
                <div className="info-box-content">
                    <span className="info-box-text">{title}</span>
                    <span className="info-box-number">{number}</span>
                </div>
            </div>
        </>
    )
}

export function SmallBox({ title = '', number = 0, fa_icon = 'fa-info', href = '#', link_text = 'buka' }) {
    return (
        <>
            <div className="small-box bg-gradient-success">
                <div className="inner">
                    <h3>{number}</h3>
                    <p>{title}</p>
                </div>
                <div className="icon">
                    <i className={'fas ' + fa_icon}></i>
                </div>
                <Link to={href} className="small-box-footer">
                    {link_text} <i className="fas fa-arrow-circle-right"></i>
                </Link>
            </div>
        </>
    )
}