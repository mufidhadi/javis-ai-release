# chcek git update
git restore *
git pull origin master

# stop all containers
sudo docker stop $(sudo docker ps -a -q)
# remove all containers
sudo docker rm $(sudo docker ps -a -q)

# remove all images
sudo docker rm image $(sudo docker images -a -q) --force

# sudo docker build -t yolo:jetson .
sudo docker build -t yolo:jetson . --no-cache