# Ultralytics YOLO 🚀, AGPL-3.0 license
# Builds ultralytics/ultralytics:jetson image on DockerHub https://hub.docker.com/r/ultralytics/ultralytics
# Supports JetPack for YOLOv8 on Jetson Nano, TX1/TX2, Xavier NX, AGX Xavier, AGX Orin, and Orin NX

# Start FROM https://catalog.ngc.nvidia.com/orgs/nvidia/containers/l4t-pytorch
FROM nvcr.io/nvidia/l4t-pytorch:r35.2.1-pth2.0-py3

# Downloads to user config dir
ADD https://ultralytics.com/assets/Arial.ttf https://ultralytics.com/assets/Arial.Unicode.ttf /root/.config/Ultralytics/

# Install linux packages
# g++ required to build 'tflite_support' and 'lap' packages, libusb-1.0-0 required for 'tflite_support' package
RUN apt update \
    && apt install --no-install-recommends -y gcc git zip curl htop libgl1 libglib2.0-0 libpython3-dev gnupg g++ libusb-1.0-0

# Create working directory
WORKDIR /usr/src/ultralytics

# Copy contents
# COPY . /usr/src/ultralytics  # git permission issues inside container
RUN git clone https://github.com/ultralytics/ultralytics -b main /usr/src/ultralytics
ADD https://github.com/ultralytics/assets/releases/download/v0.0.0/yolov8n.pt /usr/src/ultralytics/

# Remove opencv-python from Ultralytics dependencies as it conflicts with opencv-python installed in base image
RUN grep -v "opencv-python" pyproject.toml > temp.toml && mv temp.toml pyproject.toml

# Install pip packages manually for TensorRT compatibility https://github.com/NVIDIA/TensorRT/issues/2567
RUN python3 -m pip install --upgrade pip wheel
RUN pip install --no-cache tqdm matplotlib pyyaml psutil pandas onnx "numpy==1.23"
RUN pip install --no-cache -e .

# Set environment variables
ENV OMP_NUM_THREADS=1

# TAMBAHAN MUFID
WORKDIR /home

RUN pip install "lapx>=0.5.2" mysql-connector-python prettytable redis requests Flask
RUN pip install websocket-client

# # install ping
# RUN apt install -y iputils-ping

# install ffmpeg
RUN apt install -y --no-install-recommends ffmpeg

# RUN apt install -y python3-venv gcc python3-dev curl libxml2-dev libxmlsec1-dev \
# RUN apt install -y curl libxml2-dev libxmlsec1-dev \
#     && apt install --reinstall build-essential -y

# # install zerotier
# # Install curl and sudo
# RUN apt-get update && apt-get install -y curl sudo
# # Install ZeroTier One
# RUN curl https://install.zerotier.com/ | bash
# # Join the network with the given ID
# RUN zerotier-cli join 52b337794ff1ff1f

# # List the networks
# RUN zerotier-cli listnetworks


RUN git clone https://gitlab.com/mufidhadi/javis-ai-release.git
ADD https://github.com/ultralytics/assets/releases/download/v0.0.0/yolov8n.pt /home/javis-ai-release/yolov8-tracking/
# ADD https://github.com/ultralytics/assets/releases/download/v0.0.0/yolov8s.pt /home/javis-ai-release/yolov8-tracking/
# ADD https://github.com/ultralytics/assets/releases/download/v0.0.0/yolov8m.pt /home/javis-ai-release/yolov8-tracking/
# ADD https://github.com/ultralytics/assets/releases/download/v0.0.0/yolov8l.pt /home/javis-ai-release/yolov8-tracking/
# ADD https://github.com/ultralytics/assets/releases/download/v0.0.0/yolov8x.pt /home/javis-ai-release/yolov8-tracking/


# WORKDIR javis-ai-release
WORKDIR /home/javis-ai-release

RUN chmod +x install_dependencies.sh
RUN chmod +x start_pm2.sh
# RUN chmod +x ./javis-ai-release/install_dependencies.sh

# CMD [ "./install_dependencies.sh" ]


# install nodejs
RUN curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -
RUN apt install -y nodejs

# install pm2
RUN npm i -g pm2

# # install mysql server
# RUN apt update && apt install -y mysql-server

# # Start and enable MySQL service
# RUN systemctl start mysql && systemctl enable mysql

# # Secure MySQL installation (set root password and remove anonymous users)
# RUN mysql_secure_installation

# # Set MySQL password policy to low
# RUN mysql -e "SET GLOBAL validate_password.LENGTH = 4; \
#               SET GLOBAL validate_password.policy = 0; \
#               SET GLOBAL validate_password.mixed_case_count = 0; \
#               SET GLOBAL validate_password.number_count = 0; \
#               SET GLOBAL validate_password.special_char_count = 0; \
#               SET GLOBAL validate_password.check_user_name = 0; \
#               FLUSH PRIVILEGES;"

# RUN mysql -e "CREATE USER 'ai'@'%' IDENTIFIED WITH mysql_native_password BY 'JTAlbarokah313'; \
#               GRANT ALL PRIVILEGES ON *.* TO 'ai'@'%' WITH GRANT OPTION; \
#               FLUSH PRIVILEGES;"

# # PM2 manager
# RUN cd /home/javis-ai-release/pm2_manager
# RUN npm install
# RUN pm2 start index.js --name pm2_mgr --watch
# RUN pm2 save
# RUN cd /home/javis-ai-release

# # rtsp MJPEG
# RUN cd /home/javis-ai-release/rtsp_mjpeg
# # python3.10 -m venv venv
# RUN python3 -m venv venv
# RUN source venv/bin/activate
# RUN pip install -r requirements.txt
# RUN pm2 start main.py --interpreter=python --name mjpeg
# RUN pm2 save
# RUN deactivate
# RUN cd /home/javis-ai-release

# # rtsp server
# RUN cd /home/javis-ai-release/yolov8-tracking
# RUN chmod +x mediamtx
# RUN pm2 start mediamtx --name rtsp
# RUN pm2 save

# # setup database
# # RUN sudo mysql < release_db.sql

# install supervision
WORKDIR /home

RUN git clone https://github.com/roboflow/supervision.git -b 0.3.1

WORKDIR /home/supervision
RUN cp setup.py setup.py.ori
RUN grep -v "'opencv-python'," setup.py > temp-setup.py && mv temp-setup.py setup.py
RUN python3 setup.py install

WORKDIR /home/javis-ai-release/pm2_manager
RUN npm install
# RUN pm2 start index.js --name pm2_mgr --watch
# RUN pm2 save

WORKDIR /home/javis-ai-release/rtsp_mjpeg
# RUN pm2 start main.py --interpreter=python3 --name mjpeg
# RUN pm2 save

WORKDIR /home/javis-ai-release/yolov8-tracking
# RUN pm2 start storage_manager.py --interpreter=python3 --name=storage-mgr --cron-restart="0 0 * * *"
# RUN pm2 save

WORKDIR /home/javis-ai-release/streamer_ui/backend/
RUN npm install
# RUN pm2 start index.js --name=ui_backend --watch
# RUN pm2 save

WORKDIR /home/javis-ai-release/streamer_ui/frontend/
RUN npm install
RUN npm run build
# RUN pm2 serve build/ 3000 --name ui_frontend --spa --watch
# RUN pm2 save


WORKDIR /home/javis-ai-release

RUN apt install nano -y
RUN apt install mysql-client -y

ADD https://github.com/bluenviron/mediamtx/releases/download/v1.3.1/mediamtx_v1.3.1_linux_arm64v8.tar.gz mediamtx.tar.gz

RUN tar -xzf mediamtx.tar.gz

# RUN pm2 start mediamtx --name rtsp
# RUN pm2 save

# CMD [ "source","./start_pm2.sh" ]
# CMD [ "nohup", "setsid", "./start_pm2.sh" ]
CMD "./start_pm2.sh"

EXPOSE 3306
EXPOSE 3000
EXPOSE 3333
EXPOSE 3334
EXPOSE 3335
EXPOSE 3336
EXPOSE 5000
EXPOSE 5555
EXPOSE 5556
EXPOSE 6379
EXPOSE 8554