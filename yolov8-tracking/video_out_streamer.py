import argparse
import cv2
from flask import Flask, Response, request

app = Flask(__name__)

# Function to generate MJPEG frames from RTSP stream
def generate_frames(video_path):
    cap = cv2.VideoCapture(video_path)
    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break
        
        # Convert the frame to JPEG format
        ret, buffer = cv2.imencode('.jpg', frame)
        if not ret:
            continue
        
        frame_bytes = buffer.tobytes()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame_bytes + b'\r\n')
    
    cap.release()

@app.route('/video_feed')
def video_feed():
    video_path = request.args.get('video_path')
    if video_path is None:
        return "Missing 'video_path' parameter", 400
    
    return Response(generate_frames(video_path),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/')
def hello_world():
    return 'Hello, World!'

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run the Flask app to stream RTSP video.')
    parser.add_argument('-p', '--port', type=int, default=5000, help='Port number for the Flask app')
    args = parser.parse_args()
    
    app.run(host='0.0.0.0', port=args.port)
