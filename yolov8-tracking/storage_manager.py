import shutil
import os
import math
import argparse
import utils.config as cf
import requests

# pm2 start storage_manager.py --name file_cleaner --cron-restart="0 0 * * *"

FOLDER = './video_out'

MAX_GB = 100
MAX_GB = 0.1
MAX_GB = 0.05

STEP_SATUAN_DATA = 1000
STEP_SATUAN_DATA = 1024

MAX_MB = MAX_GB * STEP_SATUAN_DATA
MAX_KB = MAX_MB * STEP_SATUAN_DATA
MAX_B  = MAX_KB * STEP_SATUAN_DATA

# HTTP-API
http_url = cf.config['api']['url']

def convert_size(size_bytes):
   if size_bytes == 0:
       return "0B"
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])

def folder_stat(folder_path):
    stat_list = []
    average_size = 0
    total_size = 0
    max_size = 0
    min_size = 999999999999999999
    file_list = os.listdir(folder_path)
    if len(file_list) < 1:
        print('tidak ada file video')
        exit()
    for file in file_list:
        file_stats = os.stat(os.path.join(folder_path,file))
        stat_list.append({
            'name':file,
            'size':file_stats.st_size,
            'created':file_stats.st_ctime,
        })
        total_size += file_stats.st_size
        max_size = max(max_size,file_stats.st_size)
        min_size = min(min_size,file_stats.st_size)
    average_size = total_size / len(file_list)
    return stat_list, average_size, total_size, max_size, min_size

def delete_file(file_path):
    try:
        # Attempt to delete the file
        os.remove(file_path)
        print(f"🗑️ {file_path} terhapus")
    except OSError as e:
        # If an error occurs while deleting the file
        print(f"🪲 Error saat hapus {file_path}: {e}")

def check_and_delete(max_gb=100.0,nisab=1.75):
    if max_gb == 100.0:
        max_gb = cf.config['delete_treshold_gb']
    if nisab == 1.75:
        nisab = cf.config['file_keep_ratio']

    max_b = max_gb * STEP_SATUAN_DATA ** 3

    total, used, free = shutil.disk_usage("/")

    stat_list, average_size, total_size, max_size, min_size = folder_stat(FOLDER)

    jumlah_file_sisa = free / average_size

    print('FREE SPACE:',convert_size(free))
    print('TOTAL FILE:',convert_size(total_size))
    print('AVERAGE   :',convert_size(average_size))
    print('SISA FILE :',int(jumlah_file_sisa),'file lagi')

    # shorting stat_list based on created key
    stat_list_shorted = []
    stat_list_shorted = sorted(stat_list, key=lambda x: x['created'])

    nisab_hapus = max_b * nisab
    if total_size < nisab_hapus:
        print('✅ file masih',convert_size(total_size),' kurang dari',convert_size(nisab_hapus),'kurang dari (Nisab',nisab,'/',convert_size(nisab_hapus),') sedangkan yang ingin dihapus ada',convert_size(max_b))
        return
    # if free > max_b:
    #     if total_size < nisab_hapus:
    #         print('✅ file masih',convert_size(total_size),' kurang dari',convert_size(nisab_hapus),'kurang dari (Nisab',nisab,'/',convert_size(nisab_hapus),') sedangkan yang ingin dihapus ada',convert_size(max_b))
    #         return
    # else:
    #     print(f"💀 sisa kapasitas penyimpanan tinggal {convert_size(free)}, jadi harus segera dibersihkan {convert_size(max_b)}")
    
    # Printing the sorted list
    print('SORTED FILE LIST')
    total = 0
    total_dihapus = 0
    i = 0
    for item in stat_list_shorted:
        total += item['size']
        lebih_dari_max_b = total >= max_b
        dihapus = not lebih_dari_max_b
        # print(f"({i}) -> ({convert_size(total)}) -> (dihapus?{dihapus}) Name: {item['name']}, Size: {convert_size(item['size'])}, Created: {item['created']}")
        if (dihapus):
            delete_file(os.path.join(FOLDER, item['name']))
            total_dihapus = total

            print('hapus di dashboard...')
            # http://localhost/beacukai_camera/api/IPcamera/record_delete?file_url={URL FILES}
            file_url = 'http://'+cf.config['device_vpn_ip']+':5000/video_feed?video_path='+FOLDER+'/'+item['name']
            api_params = {
                'file_url':str(file_url),
            }
            req = requests.get(url=http_url+'api/IPcamera/record_delete',params=api_params)

        i+=1
    print(f"TOTAL UKURAN FILE DIHAPUS: {convert_size(total_dihapus)} (treshold: {convert_size(max_b)})")

def opt_parse():
    parser = argparse.ArgumentParser(
        description=f"Menghapus {MAX_GB}GB file hasil rekaman yang paling tua agar storage tetap lega",
    )
    parser.add_argument('--max_gb','-m',type=float,default=100.0,help='besarnya total file yang akan dihapus')
    parser.add_argument('--keep_ratio','-k',type=float,default=1.75,help='besarnya perbandingan besar file total dan treshold hapus yang harus dipenuhi sebelum file dihapus')
    opt = parser.parse_args()
    return opt

def main():
    opt = opt_parse()
    # check_and_delete(opt.max_gb)
    check_and_delete(opt.max_gb,opt.keep_ratio)

if __name__=='__main__':
    main()