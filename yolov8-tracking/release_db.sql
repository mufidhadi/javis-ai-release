-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.24 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for strongshort_cctv
DROP DATABASE IF EXISTS `strongshort_cctv`;
CREATE DATABASE IF NOT EXISTS `strongshort_cctv` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `strongshort_cctv`;

-- Dumping structure for table strongshort_cctv.anpr_log
DROP TABLE IF EXISTS `anpr_log`;
CREATE TABLE IF NOT EXISTS `anpr_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kamera` int(11) NOT NULL DEFAULT '0',
  `plate` char(10) DEFAULT NULL,
  `time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_kamera` (`id_kamera`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table strongshort_cctv.anpr_log: ~0 rows (approximately)
DELETE FROM `anpr_log`;
/*!40000 ALTER TABLE `anpr_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `anpr_log` ENABLE KEYS */;

-- Dumping structure for table strongshort_cctv.kamera
DROP TABLE IF EXISTS `kamera`;
CREATE TABLE IF NOT EXISTS `kamera` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `lokasi` text,
  `rtsp` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

ALTER TABLE `kamera`
	ADD COLUMN `id_dashboard` INT NULL AFTER `rtsp`;

ALTER TABLE `kamera`
	ADD COLUMN `api_endpoint` varchar(100) NULL AFTER `id_dashboard`;

-- Dumping data for table strongshort_cctv.kamera: ~8 rows (approximately)
DELETE FROM `kamera`;
INSERT INTO `kamera` (`id`, `nama`, `ip`, `lokasi`, `rtsp`, `id_dashboard`, `api_endpoint`) VALUES
	(1, 'Simpang Masjid Fixed 1',      '172.16.23.12', 'Tabalong', 'rtsp://admin:Dishubtabalong1234@172.16.23.12:554/Streaming/Channels/102/', 1,'http://172.16.23.1/api/ai'),
	(2, 'Simpang Masjid Fixed 2',      '172.16.23.13', 'Tabalong', 'rtsp://admin:Dishubtabalong1234@172.16.23.13:554/Streaming/Channels/102/', 2,'http://172.16.23.1/api/ai'),
	(3, 'Simpang Masjid Fixed 3',      '172.16.23.14', 'Tabalong', 'rtsp://admin:Dishubtabalong1234@172.16.23.14:554/Streaming/Channels/102/', 3,'http://172.16.23.1/api/ai'),
	(4, 'Simpang Masjid Fixed 4',      '172.16.23.15', 'Tabalong', 'rtsp://admin:Dishubtabalong1234@172.16.23.15:554/Streaming/Channels/102/', 4,'http://172.16.23.1/api/ai'),
	(5, 'Simpang Bank Kalsel Fixed 1', '172.16.23.17', 'Tabalong', 'rtsp://admin:Dishubtabalong1234@172.16.23.17:554/Streaming/Channels/102/', 5,'http://172.16.23.2/api/ai'),
	(6, 'Simpang Bank Kalsel Fixed 2', '172.16.23.18', 'Tabalong', 'rtsp://admin:Dishubtabalong1234@172.16.23.18:554/Streaming/Channels/102/', 6,'http://172.16.23.2/api/ai'),
	(7, 'Simpang Bank Kalsel Fixed 3', '172.16.23.19', 'Tabalong', 'rtsp://admin:Dishubtabalong1234@172.16.23.19:554/Streaming/Channels/102/', 7,'http://172.16.23.2/api/ai'),
	(8, 'Simpang Bank Kalsel Fixed 4', '172.16.23.20', 'Tabalong', 'rtsp://admin:Dishubtabalong1234@172.16.23.20:554/Streaming/Channels/102/', 8,'http://172.16.23.2/api/ai');

-- Dumping structure for table strongshort_cctv.vid_log
DROP TABLE IF EXISTS `vid_log`;
CREATE TABLE IF NOT EXISTS `vid_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kamera` int(11) NOT NULL DEFAULT '0',
  `person` int(11) DEFAULT '0',
  `car` int(11) DEFAULT '0',
  `motorcycle` int(11) DEFAULT '0',
  `bus` int(11) DEFAULT '0',
  `truck` int(11) DEFAULT '0',
  `time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_kamera` (`id_kamera`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `vid_log`
	ADD COLUMN `smp` FLOAT NULL DEFAULT '0' AFTER `truck`;

-- Dumping data for table strongshort_cctv.vid_log: ~0 rows (approximately)
DELETE FROM `vid_log`;
/*!40000 ALTER TABLE `vid_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `vid_log` ENABLE KEYS */;

-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.24 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for strongshort_cctv
CREATE DATABASE IF NOT EXISTS `strongshort_cctv` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `strongshort_cctv`;

-- Dumping structure for table strongshort_cctv.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table strongshort_cctv.users: ~2 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`) VALUES
	(1, 'mufid', '$2b$10$Qys.wd5iTNy5Te7WnMtxieO0lLIXHR.M3iFndIPVlDe/etA3aAhFG'),
	(2, 'admin', '$2b$10$lJdzs6vMezY9kuAslH.ybOGAbeX4M6/Zlog5GQVeUt3ZUlTKb7Aom'),
	(3, 'admin', '$2b$10$qWWi2.D/4GMd5Z02JafwcunqJkI/aJvA1rvI6tjwmD7p.U.CznnuS');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
