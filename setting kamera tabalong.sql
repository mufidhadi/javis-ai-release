ALTER TABLE `kamera` ADD COLUMN `api_endpoint` varchar(100) NULL AFTER `id_dashboard`;

-- Dumping data for table strongshort_cctv.kamera: ~8 rows (approximately)
DELETE FROM `kamera`;
INSERT INTO `kamera` (`id`, `nama`, `ip`, `lokasi`, `rtsp`, `id_dashboard`, `api_endpoint`) VALUES
	(1, 'Simpang Masjid Fixed 1',      '172.16.23.12', 'Tabalong', 'rtsp://admin:Dishubtabalong1234@172.16.23.12:554/Streaming/Channels/102/', 1,'http://172.16.23.1/api/ai'),
	(2, 'Simpang Masjid Fixed 2',      '172.16.23.13', 'Tabalong', 'rtsp://admin:Dishubtabalong1234@172.16.23.13:554/Streaming/Channels/102/', 2,'http://172.16.23.1/api/ai'),
	(3, 'Simpang Masjid Fixed 3',      '172.16.23.14', 'Tabalong', 'rtsp://admin:Dishubtabalong1234@172.16.23.14:554/Streaming/Channels/102/', 3,'http://172.16.23.1/api/ai'),
	(4, 'Simpang Masjid Fixed 4',      '172.16.23.15', 'Tabalong', 'rtsp://admin:Dishubtabalong1234@172.16.23.15:554/Streaming/Channels/102/', 4,'http://172.16.23.1/api/ai'),
	(5, 'Simpang Bank Kalsel Fixed 1', '172.16.23.17', 'Tabalong', 'rtsp://admin:Dishubtabalong1234@172.16.23.17:554/Streaming/Channels/102/', 5,'http://172.16.23.2/api/ai'),
	(6, 'Simpang Bank Kalsel Fixed 2', '172.16.23.18', 'Tabalong', 'rtsp://admin:Dishubtabalong1234@172.16.23.18:554/Streaming/Channels/102/', 6,'http://172.16.23.2/api/ai'),
	(7, 'Simpang Bank Kalsel Fixed 3', '172.16.23.19', 'Tabalong', 'rtsp://admin:Dishubtabalong1234@172.16.23.19:554/Streaming/Channels/102/', 7,'http://172.16.23.2/api/ai'),
	(8, 'Simpang Bank Kalsel Fixed 4', '172.16.23.20', 'Tabalong', 'rtsp://admin:Dishubtabalong1234@172.16.23.20:554/Streaming/Channels/102/', 8,'http://172.16.23.2/api/ai');